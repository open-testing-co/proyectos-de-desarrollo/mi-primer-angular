# AngularMyFirts
Creado por Henry Andres Correa Correa
h.andresc1127@gmail.com

## Prerequisitos
Tener instalado NodeJS.

## Generación
Este proyecto fue generado con [Angular CLI](https://github.com/angular/angular-cli) version 8.0.4.

## Pasos para generar
1. Instalar Angular -> npm install -g @angular/cli
2. Crear una carpeta donde se creará el proyecto
3. Crear el proyecto angular -> ng new miProyecto
4. Seleccionar el estilo con el cual se va a trabajar, yo seleccione SCSS

## Servidor de desarrollo

Ejecutar `ng serve` para un servidor de desarrollo. La aplicación se ejecuta en `http://localhost:4200/`. La aplicación será automaticamente recargada si tu cambias cualquiera de los archivos del código fuente.

## Pilares de codificacion

Ejecutar `ng generate component component-name` para generar un nuevo componente. Tambien se puede usar `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Ejecutar `ng build` para contruir el proyecto. El artefacto construido queda almacenado en  `dist/` directory. Use the `--prod` flag for a production build..

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).